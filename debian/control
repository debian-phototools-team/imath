Source: imath
Section: libs
Priority: optional
Maintainer: Debian PhotoTools Maintainers <pkg-phototools-devel@lists.alioth.debian.org>
Uploaders: Matteo F. Vescovi <mfv@debian.org>
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 dh-python,
 doxygen,
 libboost-python-dev,
 pkgconf,
 python3-breathe <!nodoc>,
 python3-dev:native, libpython3-dev,
 python3-numpy:native,
 python3-sphinx <!nodoc>,
 python3-sphinx-press-theme <!nodoc>
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://www.openexr.com
Vcs-Git: https://salsa.debian.org/debian-phototools-team/imath.git
Vcs-Browser: https://salsa.debian.org/debian-phototools-team/imath

Package: libimath-3-1-29t64
Provides: ${t64:Provides}
Replaces: libimath-3-1-29
Breaks: libimath-3-1-29 (<< ${source:Version})
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Pre-Depends:
 ${misc:Pre-Depends}
Description: Utility libraries from ASF used by OpenEXR - runtime
 Imath is a basic, light-weight, and efficient C++ representation of
 2D and 3D vectors and matrices and other simple but useful
 mathematical objects, functions, and data types common in computer
 graphics applications, including the “half” 16-bit floating-point type.
 .
 Imath also includes optional Python bindings for all types and
 functions, including optimized implementations of vector and
 matrix arrays.
 .
 This package provides the runtime libraries.

Package: libimath-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libimath-3-1-29t64 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Pre-Depends:
 ${misc:Pre-Depends}
Conflicts:
 libilmbase-dev
Provides:
 libilmbase-dev
Replaces:
 libilmbase-dev
Description: Utility libraries from ASF used by OpenEXR - development
 Imath is a basic, light-weight, and efficient C++ representation of
 2D and 3D vectors and matrices and other simple but useful
 mathematical objects, functions, and data types common in computer
 graphics applications, including the “half” 16-bit floating-point type.
 .
 Imath also includes optional Python bindings for all types and
 functions, including optimized implementations of vector and
 matrix arrays.
 .
 This package provides the development files for Imath.

Package: libimath-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Build-Profiles: <!nodoc>
Depends:
 libjs-jquery,
 sphinx-common,
 ${misc:Depends}
Description: Utility libraries from ASF used by OpenEXR - documentation
 Imath is a basic, light-weight, and efficient C++ representation of
 2D and 3D vectors and matrices and other simple but useful
 mathematical objects, functions, and data types common in computer
 graphics applications, including the “half” 16-bit floating-point type.
 .
 Imath also includes optional Python bindings for all types and
 functions, including optimized implementations of vector and
 matrix arrays.
 .
 This package provides the official documentation for Imath.

Package: python3-imath
Section: python
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends}
Provides:
 ${python3:Provides}
Description: Utility libraries from ASF used by OpenEXR - Python bindings
 Imath is a basic, light-weight, and efficient C++ representation of
 2D and 3D vectors and matrices and other simple but useful
 mathematical objects, functions, and data types common in computer
 graphics applications, including the “half” 16-bit floating-point type.
 .
 Imath also includes optional Python bindings for all types and
 functions, including optimized implementations of vector and
 matrix arrays.
 .
 This package provides Python bindings to the Imath library.
